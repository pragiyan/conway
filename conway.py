#!/usr/bin/env python 

import logging
from bitarray import bitarray

def compute_next(i, old_g, size):
	"""Computes what value should occur at `i`th index of old_g on next gen"""
	or_val = old_g[(i-1)%size] or old_g[(i+1)%size]
	not_val = not old_g[i%size]
	value = or_val and not_val
	return value


def compute(init_state, gen):
	"""Computes the state of init_state after `gen` generation"""
	old_g = bitarray(init_state)
	new_g = bitarray(init_state)
	size = len(init_state)
	for i in range(gen):
		for i in range(size):
			new_g[i] = compute_next(i, old_g, size)
		old_g = bitarray(new_g)
	return old_g


if __name__ == "__main__":
	
	_LOGGER = logging.getLogger(__name__)

	logging.basicConfig(
		level=logging.DEBUG,
		format='[%(asctime)s] %(levelname)s: %(funcName)s - %(message)s'
	)

	init_state_data = raw_input("Enter initial state: ")
	init_state_data = str(init_state_data)
	size = len(init_state_data)

	gen = input("Enter number of generations to compute: ")

	_LOGGER.info("CONWAY PROBLEM SOLUTION")
	_LOGGER.info("Data is " + init_state_data + " of size " + str(size))

	init_state = bitarray(init_state_data)
	result = compute(init_state, gen)

	_LOGGER.info("Result is: {0}".format(str(result)[-(size+2):-2])) 
